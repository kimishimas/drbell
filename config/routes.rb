Rails.application.routes.draw do

  root to: redirect("/top")

  get 'history/new'

  #MENU001
  get 'top' => 'menu#menu001'

  scope '/main' do
    #JBOH001
    get 'history' => 'history#index'
    get 'history/:id' => 'history#show'
    #JBOM002 ~ JBOM005
    get 'jobm/new' => 'jobm#new'
    get 'jobm/show' => 'jobm#show'
    post 'jobm' => 'jobm#create'
    delete 'jobm/:id' => 'jobm#destroy'
    get 'jobm/:id/execute' => 'jobm#execute', as: 'jobm_execute'

    #JBOM101 ~ JBOM104
    get 'jobm1/new' => 'jobm1#new'
    get 'jobm1/show' => 'jobm1#show'
    post 'jobm1' => 'jobm1#create'
    post 'jobm1/confirm' => 'jobm1#confirm'
    get 'jobm1/finish' => 'jobm1#finish'
    delete 'jobm1/:id' => 'jobm1#destroy'
  end

  #JOBM001
  #JOBD001
  resources :main, only: [:index, :show] do
    #JOBA001
    get 'jobcommand', to: 'main#jobcommand'
    #JOBR001
    get 'jobr/new' => 'jobr#new'
    #JOBR002
    post 'jobr/confirm' => 'jobr#confirm'
    #JOBR003
    post 'jobr' => 'jobr#create'
    #JOBR004
    get 'jobr/show' => 'jobr#show'
    delete 'jobr/:id' => 'jobr#destroy'

    #JOBD101
    get 'jobd/new' => 'jobd#new'
    #JOBDD102
    post 'jobd/confirm' => 'jobd#confirm'
    #JOBD0103
    post 'jobd' => 'jobd#create'
    #JOBD104
    get 'jobd/finish' => 'jobd#finish'
    get 'jobd/show' => 'jobd#show'
    delete 'jobd/:id' => 'jobd#destroy'
  end

  #MENU002
  get 'menu/menu002'

  scope 'setting' do
    #CONF001
    get 'conf' => 'conf#index'
    #CONF002,CONF003
    get 'conf/:id' => 'conf#show'
    post 'conf/:id' => 'conf#update', as: 'conf_update'
    get 'conf/:id/done' => 'conf#done', as: 'conf_done'

    #CONF101
    get 'conf1' => 'conf1#index'
    get 'conf1/new' => 'conf1#new'
    post 'conf1/new' => 'conf1#create'
    get 'conf1/:id' => 'conf1#show'
    post 'conf1/:id' => 'conf1#update'
    post 'conf1' => 'conf1#update'
    delete 'conf1/:id' => 'conf1#destroy'

    #CONF201
    get 'conf2' => 'conf2#index'
    post 'conf2' => 'conf2#update'
    post 'conf2/search' => 'conf2#search'
    get 'conf2/search' => 'conf2#search'
    post 'conf2/new' => 'conf2#new'
    delete 'conf2' => 'conf2#destroy'
    get 'download' => 'conf2#download'

    #CONF202
    get 'viewlog' => 'conf2#history'
    get 'downloadlog' => 'conf2#downloadlog'
  end

end
