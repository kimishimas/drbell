require 'test_helper'

class JobmControllerTest < ActionDispatch::IntegrationTest
  test "should get jobm001" do
    get jobm_jobm001_url
    assert_response :success
  end

  test "should get jobm002" do
    get jobm_jobm002_url
    assert_response :success
  end

  test "should get jobm003" do
    get jobm_jobm003_url
    assert_response :success
  end

  test "should get jobm004" do
    get jobm_jobm004_url
    assert_response :success
  end

  test "should get jobm005" do
    get jobm_jobm005_url
    assert_response :success
  end

end
