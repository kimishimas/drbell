require 'test_helper'

class JobrControllerTest < ActionDispatch::IntegrationTest
  test "should get jobr001" do
    get jobr_jobr001_url
    assert_response :success
  end

  test "should get jobr002" do
    get jobr_jobr002_url
    assert_response :success
  end

  test "should get jobr003" do
    get jobr_jobr003_url
    assert_response :success
  end

  test "should get jobr004" do
    get jobr_jobr004_url
    assert_response :success
  end

end
