require 'test_helper'

class Conf1ControllerTest < ActionDispatch::IntegrationTest
  test "should get conf101" do
    get conf1_conf101_url
    assert_response :success
  end

  test "should get conf102" do
    get conf1_conf102_url
    assert_response :success
  end

end
