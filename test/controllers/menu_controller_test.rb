require 'test_helper'

class MenuControllerTest < ActionDispatch::IntegrationTest
  test "should get menu001" do
    get menu_menu001_url
    assert_response :success
  end

  test "should get menu002" do
    get menu_menu002_url
    assert_response :success
  end

end
