require 'test_helper'

class JobdControllerTest < ActionDispatch::IntegrationTest
  test "should get jobd001" do
    get jobd_jobd001_url
    assert_response :success
  end

  test "should get jobd101" do
    get jobd_jobd101_url
    assert_response :success
  end

  test "should get jobd102" do
    get jobd_jobd102_url
    assert_response :success
  end

  test "should get jobd103" do
    get jobd_jobd103_url
    assert_response :success
  end

  test "should get jobd104" do
    get jobd_jobd104_url
    assert_response :success
  end

end
