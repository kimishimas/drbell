require 'test_helper'

class ConfControllerTest < ActionDispatch::IntegrationTest
  test "should get conf001" do
    get conf_conf001_url
    assert_response :success
  end

  test "should get conf002" do
    get conf_conf002_url
    assert_response :success
  end

  test "should get conf003" do
    get conf_conf003_url
    assert_response :success
  end

end
