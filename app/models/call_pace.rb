class CallPace < ApplicationRecord
  self.table_name = 'call_paces'
  self.primary_key = :cp_id

  has_many :gateways, foreign_key: :cp_id

  ## Validation
  # 発呼ペース名
  validates :description, presence: { message: 'を入力してください。' }
  validates :description, length: { maximum: 20, message: 'の入力が不正です。' }

  # 発呼ペース0-23
  validates :cp_0, :cp_1, :cp_2, :cp_3, :cp_4, :cp_5, :cp_6, :cp_7, :cp_8, :cp_9, :cp_10, :cp_11,
    :cp_12, :cp_13, :cp_14, :cp_15, :cp_16, :cp_17, :cp_18, :cp_19, :cp_20, :cp_21, :cp_22, :cp_23,
    presence: { message: 'を設定してください。' }

  validates :cp_0, :cp_1, :cp_2, :cp_3, :cp_4, :cp_5, :cp_6, :cp_7, :cp_8, :cp_9, :cp_10, :cp_11,
    :cp_12, :cp_13, :cp_14, :cp_15, :cp_16, :cp_17, :cp_18, :cp_19, :cp_20, :cp_21, :cp_22, :cp_23,
		format: {with: /\A[0-9]*\z/, message: 'は半角数字で入力してください。'}

  validates :cp_0, :cp_1, :cp_2, :cp_3, :cp_4, :cp_5, :cp_6, :cp_7, :cp_8, :cp_9, :cp_10, :cp_11,
    :cp_12, :cp_13, :cp_14, :cp_15, :cp_16, :cp_17, :cp_18, :cp_19, :cp_20, :cp_21, :cp_22, :cp_23,
    numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 999,
      message: 'は1 - 999の間で設定してください。', allow_blank: true }

end
