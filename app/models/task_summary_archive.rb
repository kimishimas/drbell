class TaskSummaryArchive < ApplicationRecord
  self.table_name = 'task_summaries_archive'
  self.primary_key = :jod_id, :carrier_type_cd

  ##Validation 
  #
  validates :job_id, presence: true
  validates :job_id, length: { maximum: 20 }

  validates :carrier_type_cd, presence: true
  validates :carrier_type_cd, numericality: { only_integer: true }

  validates :carrier_type_desc, presence: true
  validates :carrier_type_desc, length: { maximum: 20 }

  validates :ok_recs, numericality: { only_integer: true }
end
