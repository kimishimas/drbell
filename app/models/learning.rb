class Learning < ApplicationRecord
  self.table_name = 'learning'
  self.primary_key = :telnum

  belongs_to :carrier, primary_key: :type_cd, foreign_key: :carrier_type_cd

  ##Validation
  #
  validates :telnum, presence: true
  validates :telnum, uniqueness: true
  validates :telnum, length: { maximum: 14 }

  validates :carrier_type_cd, presence: true
  validates :carrier_type_cd, numericality: { only_integer: true }

  validates :updated_at, presence: true
end
