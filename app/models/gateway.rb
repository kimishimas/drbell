class Gateway < ApplicationRecord
  self.table_name = 'gateways'
  self.primary_key = :gw_id

  belongs_to :calling_controller, primary_key: [:cc_id], foreign_key: [:cc_id]
  belongs_to :nic, primary_key: [:cc_id, :description], foreign_key: [:cc_id, :cc_nic_desc]
  has_many :gw_telnums, foreign_key: :gw_id
  belongs_to :call_pace, primary_key: :cp_id, foreign_key: :cp_id

  accepts_nested_attributes_for :gw_telnums, allow_destroy: true

  ##Validate
  #
  validates :gw_id, presence: true
  validates :gw_id, uniqueness: true
  validates :gw_id, numericality: { only_integer: true }
  validates :cc_id, length: { maximum: 16 }
  validates :cc_nic_desc, length: { maximum: 8 }
  validates :description, presence: true
  validates :description, length: { maximum: 20 }
  validates :ip_addr, length: { maximum: 39 }
  validates :channels, presence: true
  validates :channels, numericality: { only_integer: true }
  validates :cp_id, numericality: { only_integer: true }
  validates :sc_id, length: { maximum: 16 }
  validates :enable, inclusion: { in: [true, false] }
end
