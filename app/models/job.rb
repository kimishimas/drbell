class Job < ApplicationRecord
  enum priority: { 月次: 0, 普通: 1, 高: 2, 急: 3 }
  enum type_cd: { 月次ジョブ: 0, 受託ジョブ: 1 }

  self.table_name = 'jobs'
  self.primary_key = :job_id

  belongs_to :job_status, primary_key: :status_cd, foreign_key: :status
  has_many :load_mt_st1s
  has_many :load_mt_st2s
  has_many :load_ods
  has_many :inp_mts
  has_many :inp_ods
  has_many :prcss_alls
  has_many :telnum_files
  has_many :task_summaries
  has_many :ope_requests, foreign_key: :ope_job_id

  ##Validation
  #
  validates :job_id, presence: true
  validates :job_id, uniqueness: true
  validates :job_id, length: { maximum: 20 }

  validates :type_cd, presence: true
  validates :type_cd, numericality: { only_integer: true }

  validates :description, presence: true
  validates :description, length: { maximum: 20 }

  validates :staff_id, length: { maximum: 15 }

  validates :staff_nm, presence: true
  validates :staff_nm, length: { maximum: 20 }

  validates :status, presence: true
  validates :status, numericality: { only_integer: true }

  validates :priority, presence: true
  validates :priority, numericality: { only_integer: true }

  validates :inner_pri, numericality: { only_integer: true }, allow_nil: true

  validates :created_at, presence: true, allow_nil: true

  validates :alltask_recs, numericality: { only_integer: true }

  validates :upd_recs, numericality: { only_integer: true }
end
