class NavailList < ApplicationRecord
  self.table_name = 'navail_list'
  self.primary_key = :na_id
  ##Validation

  validates :telnum_exp, presence: { message: 'を入力してください。' }
  validates :telnum_exp, length: { maximum: 14, message: 'は14字以内で入力してください。' }
  validates :telnum_exp, format: { with: /\A[0-9%_]*\z/, message: 'は数字と"%","_"のみで入力してください。' }

  validates :remarks, length: { maximum: 30, message: 'は30字以内で入力してください。' }

  validates :enabled_flg, inclusion: { in: [true, false], message: 'の入力が不正です。' }
end
