class OpeRequest < ApplicationRecord
  self.table_name = 'ope_requests'
  self.primary_key = :ope_job_id

  belongs_to :job, foreign_key: :ope_job_id

  ##Validation
  #
  validates :id, presence: true
  validates :id, uniqueness: true

  validates :staff_id, presence: true
  validates :staff_id, length: { maximum: 15 }

  validates :ope_type_cd, presence: true
  validates :ope_type_cd, numericality: { only_integer: true }

  validates :ope_job_id, presence: true
  validates :ope_job_id, length: { maximum: 20 }

  validates :created_at, presence: true, allow_nil: true
end
