class Carrier < ApplicationRecord
  self.table_name = 'carriers'
  self.primary_key = :type_cd

  has_many :calling_controllers, foreign_key: :carrier_type_cd
  has_many :task_summaries, foreign_key: :carrier_type_cd
  has_many :learnings, foreign_key: :carrier_type_cd
  has_many :area_codes, foreign_key: :carrier_type_cd

  ##Validate
  validates :type_cd, numericality: { only_integer: true }
  validates :description, presence: true
  validates :description, length: { maximum: 20 }
  validates :description, length: { maximum: 20 }
  validates :def_flg, inclusion: { in: [true, false] }
end

