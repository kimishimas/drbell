class InpMt < ApplicationRecord
  self.table_name = 'inp_mt'
  self.primary_key = :job_id, :ser_injob

  belongs_to :job

  ##Validation
  #
  validates :job_id, presence: true
  validates :job_id, length: { maximum: 20 }

  validates :ser_injob, presence: true
  validates :ser_injob, numericality: { only_integer: true }

  validates :telnum, presence: true
  validates :telnum, length: { maximum: 14 }

  validates :carrier_type_cd, presence: true
  validates :carrier_type_cd, numericality: { only_integer: true }
end
