class GwTelnum < ApplicationRecord
  self.table_name = 'gw_telnum'
  self.primary_key = :gw_id, :telnum

  belongs_to :gateway, foreign_key: :gw_id

  ##Validation

  validates :gw_id, presence: true
  # validates :gw_id, uniqueness: true
  validates :gw_id, numericality: { only_integer: true }

  validates :telnum, presence: true
  validates :telnum, length: { maximum: 14 }
end
