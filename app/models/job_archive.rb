class JobArchive < ApplicationRecord
  self.table_name = 'job_archive'
  self.primary_key = 'job_id'

  has_many :task_summary_archives, foreign_key: 'job_id'
  belongs_to :job_status, primary_key: :status_cd, foreign_key: :status

  ##Validation
  #
  validates :job_id, presence: true
  validates :job_id, uniqueness: true
  validates :job_id, length: { maximum: 20 }

  validates :type_cd, presence: true
  validates :type_cd, numericality: { only_integer: true }

  validates :description, presence: true
  validates :description, length: { maximum: 20 }

  validates :staff_id, presence: true
  validates :staff_id, length: { maximum: 15 }

  validates :staff_nm, length: { maximum: 20 }

  validates :status, presence: true
  validates :status, numericality: { only_integer: true }

  validates :created_at, presence: true

  validates :all_recs, numericality: { only_integer: true }

  validates :ok_recs, numericality: { only_integer: true }

  validates :err_recs, numericality: { only_integer: true }

  validates :blk_recs, numericality: { only_integer: true }
end
