class AreaCode < ApplicationRecord
  self.table_name = 'area_codes'
  self.primary_key = :area_code

  belongs_to :carrier, primary_key: :type_cd, foreign_key: :carrier_type_cd

  ##Validation
  #
  validates :area_code, presence: true
  validates :area_code, uniqueness: true
  validates :area_code, length: { minimum: 1, maximum: 14 }

  validates :carrier_type_cd, numericality: { only_integer: true }
end
