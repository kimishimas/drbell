class Nic < ApplicationRecord
  self.table_name = 'nics'
  self.primary_key = :cc_id, :description

  belongs_to :calling_controller, foreign_key: :cc_id
  has_many :gateways, primary_key: [:cc_id, :description], foreign_key: [:cc_id, :cc_nic_desc]

  ##Validation
  validates :cc_id, presence: true
  validates :cc_id, length: { maximum: 16 }

  validates :description, presence: true
  validates :description, length: { maximum: 8 }
end
