class LoadOd < ApplicationRecord
  self.table_name = 'load_od'
  self.primary_key = :job_id

  belongs_to :job

  ##Validation
  #
  validates :job_id, presence: true
  validates :job_id, length: { maximum: 20 }

  validates :ser_injob, presence: true
  validates :ser_injob, numericality: { only_integer: true }

  validates :telnum, presence: true
  validates :telnum, length: { maximum: 14 }

  validates :created_at, presence: true
end
