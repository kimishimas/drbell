class TelnumFile < ApplicationRecord
  self.table_name = 'telnum_files'
  self.primary_key = :job_id, :filename

  belongs_to :job

  ##Validation
  #
  validates_uniqueness_of :filename, :scope => [:job_id], message: "に既に登録されているファイルがあります。"


  validates :job_id, presence: true
  validates :job_id, length: { maximum: 20 }

  validates :filename, presence: true
  validates :filename, length: { maximum: 25 }

  validates :loaded, inclusion: { in: [true, false] }

  validates :created_at, presence: true, allow_nil: true
end
