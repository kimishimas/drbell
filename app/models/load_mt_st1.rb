class LoadMtSt1 < ApplicationRecord
  self.table_name = 'load_mt_st1'
  self.primary_key = :job_id

  belongs_to :job

  ##Validation
  #
  validates :job_id, presence: true
  validates :job_id, length: { maximum: 20 }

  validates :telnum, presence: true
  validates :telnum, length: { maximum: 14 }
end
