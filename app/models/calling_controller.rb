class CallingController < ApplicationRecord
  self.table_name = 'calling_controllers'
  self.primary_key = :cc_id

  belongs_to :carrier, primary_key: :type_cd, foreign_key: :carrier_type_cd
  has_many :nics, foreign_key: :cc_id
  has_many :gateways, foreign_key: :cc_id

  accepts_nested_attributes_for :gateways

  ##Validation
  #
  validates :cc_id, presence: true
  validates :cc_id, uniqueness: true
  validates :cc_id, length: { maximum: 16 }

  validates :carrier_type_cd, numericality: { only_integer: true, allow_blank: true }

  validates :ip_addr, length: { maximum: 39 }
end
