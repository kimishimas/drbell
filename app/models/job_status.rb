class JobStatus < ApplicationRecord
  self.table_name = 'job_status'
  self.primary_key = :status_cd

  has_many :jobs, primary_key: :status_cd, foreign_key: :status

  ##Validation
  #
  validates :status_cd, presence: true
  validates :status_cd, uniqueness: true
  validates :status_cd, numericality: { only_integer: true }

  validates :description, presence: true
  validates :description, length: { maximum: 10 }
end
