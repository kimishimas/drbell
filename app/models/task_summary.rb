class TaskSummary < ApplicationRecord
  self.table_name = 'task_summaries'
  self.primary_key = :job_id, :carrier_type_cd

  belongs_to :carrier, primary_key: :type_cd, foreign_key: :carrier_type_cd
  belongs_to :job

  ##Validation
  #
  validates :job_id, presence: true
  validates :job_id, length: { maximum: 20 }

  validates :carrier_type_cd, presence: true
  validates :carrier_type_cd, numericality: { only_integer: true }

  validates :ok_recs, numericality: { only_integer: true }

  validates :err_recs, numericality: { only_integer: true }
end
