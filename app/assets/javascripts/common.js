document.addEventListener("turbolinks:load", function () {
    if ( $.modal !== undefined ) {
      $.modal.defaults = {
        escapeClose: false,
        clickClose: true,
        showClose: false
      };
    }

    $('[data-gw-telnums]')
        .on('click', '[data-add-gw-telnum]', function () {
            var $gwTelnums = $(this).parents('[data-gw-telnums]');
            var $input = $(this).parents('tr').find('input.add_gw_telnum');
            var number = $input.val();
            var $errorExplanation = $gwTelnums.find(".error_explanation");

            $errorExplanation.html('');
            if (! $input.get(0).reportValidity() ) {
              return false;
            } else if (number === "") {
              $errorExplanation.html("<ul><li>電話番号を入力してください</li></ul>");
              return false;
            } else if (! /\d+/.test(number)) {
              $errorExplanation.html("<ul><li>電話番号は数値で入力してください</li></ul>");
              return false;
            }

            $(this).parents('tr').before("<tr><td></td><td><input type='text' class='form-control' name='calling_controller[gateways_attributes][][gw_telnums_attributes][][telnum]' value='" + number + "' readonly></td> <td><button type='button' class='btn btn-primary' data-remove-gw-telnum>削除</button></td> </tr>")

            $input.val("");
            $input.focus();
        })

        .on('click', '[data-remove-gw-telnum]', function () {
            $(this).parents('tr').remove();
        })

        .on('click', '[data-remove-gw-telnums]', function () {
          var $gwTelnums = $(this).parents('[data-gw-telnums]');
          var $errorExplanation = $gwTelnums.find(".error_explanation");
          $errorExplanation.html('');

          var $checkedTelnums = $gwTelnums.find('input[type=checkbox]:checked');

          if ( $checkedTelnums.size() === 0 ) {
            $errorExplanation.html("<ul><li>電話番号を選択してください</li></ul>");
            return false;
          }
          $checkedTelnums.each(function () {
            $(this).parents('tr').append("<input type='hidden' name='calling_controller[gateways_attributes][][gw_telnums_attributes][][_destroy]' value='1'>")
            $(this).parents('tr').css('display', 'none');
          });

          closeModal(this);
          return false;
        })

        .on('click', '[data-close-modal]', function () {
          closeModal(this);
          return false;
        });

    var closeModal = function(target) {
      var $modal = $(target).parents('.modal');
      var $id = $modal.attr('id');
      $('a[href=\\#' + $id + ']').after($modal);
      $.modal.close();
    };
});
