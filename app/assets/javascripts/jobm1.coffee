$(document).on 'turbolinks:load', ->

  _$mode = 'confirm'

  _$errorMsg = ''

  # ファイル名を配列にて格納
  $(".new_addfiles input[type=file]").each(->
    _$name = $(this).attr("name")
    $(this).attr("name", _$name + '[]')
    return
  )

  # ファイル追加ボタン処理
  $("#jobm1_new_addfile_btn").on("click", ->
    if $("#file_csv").val() == "" || $("#job_file_csv").val() == ""
      alert("ファイルを選択してから押してください。")
      return
    if $(".new_addfiles").length < 5
      _$tr = $(".new_addfiles").last()
      _$clone = _$tr.clone(true)
      _$clone.children().eq(1).find("input.addfile").val("")
      _$clone.insertAfter(_$tr)
      if $(".new_addfiles").length > 1
        $(".jobm1_new_delfile_btn").css("display", "")
    return
  )
  # ファイル削除ボタン処理
  $(".jobm1_new_delfile_btn").each(->
    $(this).on("click", ->
      if $(".new_addfiles").length > 1
        $($(this).closest("tr").remove()).remove();
        if $(".new_addfiles").length == 1
          $(".jobm1_new_delfile_btn").css("display", "none")
      return
    )
  )

  # 画面の表示切替処理
  _DisplayProc = ->
   switch _$mode
     when 'new'
       _NewDisplay()
     when 'confirm'
       if _ConfirmCheck()
         _ConfirmDisplay()
       else
         $("#errorMsg").html(_$errorMsg)
     when 'regist'
       _RegistDisplay()
   return false

  # 登録(入力)画面の表示
  _NewDisplay = ->
    current_menu = $(".table-info")
    current_menu.removeClass("table-info")
    current_menu.addClass("table-active")
    current_menu.prev().addClass("table-info")
    current_menu.prev().removeClass("table-active")
    # ファイル追加ボタンの表示
    $("#jobm1_new_addfile_btn").css("display", "")
    $(".jobm1_new_delfile_btn").css("display", "")

    $("input[type=text], input[type=file], input[type=checkbox], select").each(->
      $(this).css("display", "")
      return
    )

    $(".confirm-view").remove()
    $("#new_button_group").css("display", "block")
    $("#confirm_button_group").css("display", "none")

    return

  # 確認画面の表示
  _ConfirmDisplay = ->

    current_menu = $(".table-info")
    current_menu.removeClass("table-info")
    current_menu.addClass("table-active")
    current_menu.next().addClass("table-info")
    current_menu.next().removeClass("table-active")

    # エラーメッセージの非表示
    $("#errorMsg").html("")
    $("p.notice").css("display", "none")

    # ファイル追加ボタンの非表示
    $("#jobm1_new_addfile_btn").css("display", "none")
    $(".jobm1_new_delfile_btn").css("display", "none")
    $("input[type=text]").each(->
      $(this).css("display", "none")
      $("<span>").addClass("confirm-view").text($(this).val()).insertAfter($(this))
      return
    )
    $("input[type=file]").each(->
      if this.files.length > 0
        $(this).css("display",  "none")
        $("<span>").addClass("confirm-view").text(this.files[0].name).insertAfter($(this))
      else
        $(this).parents("tr").remove()
      return
    )
    $("select").each(->
      $(this).css("display", "none")
      $("<span>").addClass("confirm-view").text($(this).find('option:selected').text()).insertAfter($(this))
      return
    )

    $("input[type=checkbox]").css("display", "none")
    if $("input[type=checkbox]").prop("checked")
      $("<span>").addClass("confirm-view").text("登録後、即時実行します。").insertAfter($("input[type=checkbox]"))

    # ボタングループの表示切替
    $("#new_button_group").css("display", "none")
    $("#confirm_button_group").css("display", "block")
    return

  # 処理中画面
  _RegistDisplay = ->
    current_menu = $(".table-info")
    current_menu.removeClass("table-info")
    current_menu.addClass("table-active")
    current_menu.next().addClass("table-info")
    current_menu.next().removeClass("table-active")
    $("form").css("display", "none")
    $("#regist_display").css("display", "block")
    $("#confirm_button_group").css("display", "none")
    $("form").submit()
    return

  # 送信内容の確認処理
  _ConfirmCheck = ->
    nameAry = []
    result = true
    _$errorMsg = ""
    file_find = false
    file_unique = true
    file_maxlength = true
    file_valid_chars = true
    $(".new_addfiles input[type=file]").each(->
      if this.files.length > 0
        if $.inArray(this.files[0].name, nameAry) == -1
          nameAry.push(this.files[0].name)
          file_find = true
          if this.files[0].name.length > 25
            file_maxlength = false
        else
          file_unique = false
        #不正文字チェック
        if /^[0-9A-Za-z-_.]*$/.test(this.files[0].name) == false
          file_valid_chars = false
      return
    )
    if !$("#job_description").val()?
      if file_unique == false
        _$errorMsg += "<li>同じファイル名が指定されています。</li>"
      else if file_find == false
        _$errorMsg += "<li>調査対象電話番号リストファイルが選択されていません。</li>"
      else if file_maxlength == false
        _$errorMsg += "<li>ファイル名が長すぎます。</li>"
      else if file_valid_chars == false
        _$errorMsg += "<li>ファイル名に不正な文字が含まれています。</li>"
    else
      if $("#job_description").val().length == 0
        _$errorMsg += "<li>ジョブ名が入力されていません。</li>"
      else if $("#job_description").val().length > 20
        _$errorMsg += "<li>ジョブ名が長すぎます。</li>"
      else if /\\|\/|\:|\*|\?|\"|\<|\>|\|/.test($("#job_description").val())
        _$errorMsg += "<li>ジョブ名に禁止文字が含まれています。</li>"
      if $("#job_staff_nm").val().length == 0
        _$errorMsg += "<li>登録者名が入力されていません。</li>"
      else if $("#job_staff_nm").val().length > 20
        _$errorMsg += "<li>登録者名が長すぎます。</li>"
      if file_unique == false
        _$errorMsg += "<li>同じファイル名が指定されています。</li>"
      else if file_find == false
        _$errorMsg += "<li>調査対象電話番号リストファイルが選択されていません。</li>"
      else if file_maxlength == false
        _$errorMsg += "<li>ファイル名が長すぎます。</li>"
      else if file_valid_chars == false
        _$errorMsg += "<li>ファイル名に不正な文字が含まれています。</li>"

    if _$errorMsg != ""
      _$errorMsg = "<ul>" + _$errorMsg + "</ul>"
      result = false
    return result

  # ボタンによる画面制御 (確認画面)
  $(".data-mode-button").on("click", ->
    _$mode = $(this).data("mode")
    if typeof _$mode == 'undefined'
      _$mode = 'confirm'
    _DisplayProc()
    return false
  )
