require 'csv'

CSV.generate(:encoding => "SJIS", row_sep: "\r\n") do |csv|
	csv_columns_names = ["自粛リスト", "説明", "無効"]
	csv << csv_columns_names
	@navail_csv.each do |navail|
		if navail.enabled_flg == true then
			flg = 0
		else
			flg = 1
		end
		csv_column_values = [
			navail.telnum_exp,
			navail.remarks,
			flg
		]
		csv << csv_column_values
	end
end
