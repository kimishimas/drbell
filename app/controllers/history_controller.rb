class HistoryController < ApplicationController
	require  "date"
	JOBTYPE_MONTH = 0
	JOBTYPE_ACCESS = 1
	def index
		month = Date.today.strftime("%m").to_i
		redirect_to controller: 'history', action: 'show' ,id: month
	end

  def show
		target = params[:id].to_i
		this_year = Date.today.strftime("%Y").to_i
		@this_month = Date.today.strftime("%m").to_i
		if target < 1 || target > 12
			target = @this_month
		end
		if @this_month >= target
			time = Time.new(this_year, target)
		else
			time = Time.new(this_year - 1, target)
		end
		range_s = time.beginning_of_month
		range_e = time.end_of_month
		@jobarchive = JobArchive.all
		#受託ジョブ
		@jobarchive_acces = JobArchive.where(type_cd: JOBTYPE_ACCESS, created_at: range_s..range_e).order('created_at DESC')
		#月次ジョブ
		@jobarchive_month = JobArchive.where(type_cd: JOBTYPE_MONTH, created_at: range_s..range_e).order('created_at DESC')
		#月次ジョブのキャリア種別
		#キャリア種別取得
		carriers = Carrier.where().not(type_cd: -1)
		@carriers_statistics = []
		carriers.each do |f|
			if f.type_cd == -1
				next
			end
			@carriers_statistics[f.type_cd] = {:desc => f.description, :tot => 0}
		end
		@jobarchive_month.each do |f|
			f.task_summary_archives.each do |g|
				@carriers_statistics[g.carrier_type_cd][:tot] += g.ok_recs
			end
		end
		#その他を最後へ
		@carriers_statistics[@carriers_statistics.length] = @carriers_statistics[0]
		@carriers_statistics.delete_at(0)

		#月リスト作成
		@month_ary = []
		this_year -= 2000
		for m in 1..12
			m += @this_month
			if m > 12
				m -= 12
			end
			if m == 1
				desc = "#{this_year}年1月"
				this_year -= 1
			else
				desc = "#{m}月"
			end
			if m == target
				@month_ary.push({:month => m, :desc => desc, :active => true})
			else
				@month_ary.push({:month => m, :desc => desc, :active => false})
			end
		end
		if @this_month != 12
			@month_ary[0] = {:month => @month_ary[0][:month], :desc => "#{this_year}年#{@month_ary[0][:month]}月", :active => @month_ary[0][:active]}
		end
		
  end
end
