class JobrController < ApplicationController
  #before_action :set_, only: [:show, :edit, :update, :destroy]

  #JOBR001
  def new
		@carriers = Carrier.where.not(type_cd: -1)
		@job_id = params[:main_id]
		@job = Job.where(job_id: @job_id).first
  end

  #JOBR002
  def confirm
    c_type = params[:c_values].strip!
    @job = Job.where(job_id: params[:main_id]).first
    date = Date.today.strftime("%Y%m%d")
		
    #出力カラム選定
    sql = "SELECT ser_injob,"
    column = ""
    if params[:tln] == "true"
	sql += "telnum, "
	column += "電話番号,"
    end
    #日付フォーマット判別
    dateflg = 0
    if params[:c_y] == "true" || params[:c_h] == "true"
        sql += "call_ymd, "
	if params[:c_y] == "true" && params[:c_h] != "true"
	    column += "発呼日,"
	    dateflg = 1
	elsif params[:c_y] != "true" && params[:c_h] == "true"
	    column += "発呼時間,"
	    dateflg = 2
	else
	    column += "発呼日,発呼時間,"
	    dateflg = 3
	end
    end
    if params[:c_t_c] == "true"
	sql += "carrier_type_cd, "
	column += "キャリア種別名,"
    end
    if params[:e_c] == "true"
	sql += "err_cd, "
	column += "エラーコード,"
    end
    if params[:w_c] == "true"
	sql += "warn_cd, "
	column += "ワーニングコード,"
    end
    if params[:s_c] == "true"
	sql += "sts_cd, "
	column += "ステータスコード"
    end

    @files = []
    sql = sql.slice(0..-3)
    sql += " FROM res_#{@job.job_id} WHERE carrier_type_cd = "
    column = column.slice(0..-1)
    c_type.split(" ").each do |cd| 
    #キャリアごとに書込み
    c_des = Carrier.where(type_cd: cd).first
    filename = "Result_#{@job.description}_#{c_des.description}_#{date}_#{session[:staff_id]}.csv"
    filename = filename.gsub(/\//, '_').gsub(/\\/, '_')
    @files.push(escapeUrl(filename))
    #20180405 addcode winney
    #records = ActiveRecord::Base.connection.select_all(sql + cd)
    ser_injob = 0
    #limit = records.length
    File.open(REPORT + filename, "w", :encoding => "SJIS") do |file|
	file.write(column + "\r\n" )
	#for conter in 0..limit do
	recordLength = 0
	cd_sql = cd;	
	cd_sql += " and ser_injob > "
	flg_notavail = false
	begin
	    listrecords = ActiveRecord::Base.connection.select_all(sql + cd_sql+ ser_injob.to_s + " order by carrier_type_cd desc, ser_injob asc limit 1000")
	    #listrecords.collect {|x| x.each do |record|
	    #listrecords.each do |record|
	    if listrecords.length == 0
		break
	    end
	    listrecords.each do |record|
	    line = ""
	    record.each {|k,v|
	        if k == "call_ymd" and v != nil
	            date_ary = v.split(" ")
	            c_y = date_ary[0].gsub!(/-/, '')
	            c_h = date_ary[1].split(".")
	            c_h = c_h[0].gsub!(/:/, '')
	            if dateflg == 1
	                line += c_y
	            elsif dateflg == 2
	                line += c_h
	            elsif dateflg == 3
	                line += c_y + "," + c_h
	            end
	        elsif k == "call_ymd" and v == nil
	            if dateflg == 3
	                line += ","
	            end
	        elsif k == "carrier_type_cd"
	            line += c_des.description
	        elsif k == "sts_cd"
	            if flg_notavail
	                line += "30"
	            else
	                line += v.to_s
	            end
	        elsif k == "ser_injob"
	            ser_injob = v.to_s
		    next
	        else
	            line += v.to_s
	        end
	        line += ","
	    }
	    line = line.slice(0..-2)
				
	    file.write(line + "\r\n")
	    end
	    #}
	end while true
	if cd == "0"
	    cd_sql = "'-1' and ser_injob >"
	    c_notavail = Carrier.where(type_cd: "-1").first
	    ser_injob = 0
	    begin
	        listrecords = ActiveRecord::Base.connection.select_all(sql + cd_sql+ ser_injob.to_s + " order by carrier_type_cd desc, ser_injob asc limit 1000")
	        #listrecords.collect {|x| x.each do |record|
	        #listrecords.each do |record|
	        if listrecords.length == 0
		    break
	        end
	        listrecords.each do |record|
	        line = ""
	        record.each {|k,v|
	            if k == "call_ymd" and v != nil
	                date_ary = v.split(" ")
	                c_y = date_ary[0].gsub!(/-/, '')
	                c_h = date_ary[1].split(".")
	                c_h = c_h[0].gsub!(/:/, '')
	                if dateflg == 1
	                    line += c_y
	                elsif dateflg == 2
	                    line += c_h
	                elsif dateflg == 3
	                    line += c_y + "," + c_h
	                end
	            elsif k == "call_ymd" and v == nil
	                if dateflg == 3
	                    line += ","
	                end
	            elsif k == "carrier_type_cd"
	                if v == -1 
	                    flg_notavail = true	
	                else
	                    flg_notavail = false
	                end
	                if flg_notavail
	                    line += c_notavail.description
	                else
	                    line += c_des.description
	                end
	            elsif k == "sts_cd"
	                if flg_notavail
	                    line += "30"
	                else
	                    line += v.to_s
	                end
	            elsif k == "ser_injob"
	                ser_injob = v.to_s
			next
	            else
	                line += v.to_s
	            end
	            line += ","
	        }
	        line = line.slice(0..-2)
				
	        file.write(line + "\r\n")
	        end
	        #}
	    end while true
 
	end
	#20180405 addcode winney 
     end
  end
  end

  #JOBR003 not page view
  def create
  end

  #JOBR004
  def show
  end

  def update
  end

  def destroy
  end

  private
  def set_
    #@ = .find(params[:id])
  end

  def job_params
    #params.require(:).permit(:, :, :, :)
  end

  def escapeUrl(url)
    if url.nil?
	return url
    end
    url = url.gsub(/%/,  '%25')
    url = url.gsub(/\s/,  '%20')
    url = url.gsub(/!/,  '%21')
    url = url.gsub(/"/,  '%22')
    url = url.gsub(/#/,  '%23')
    url = url.gsub(/\$/, '%24')
    url = url.gsub(/&/,  '%26')
    url = url.gsub(/'/,  '%27')
    url = url.gsub(/\(/, '%28')
    url = url.gsub(/\)/, '%29')
    url = url.gsub(/\*/, '%2A')
    url = url.gsub(/\+/, '%2B')
    url = url.gsub(/,/,  '%2C')
    url = url.gsub(/-/,  '%2D')
    url = url.gsub(/\./, '%2E')
    url = url.gsub(/\//, '%2F')
    url = url.gsub(/:/,  '%3A')
    url = url.gsub(/;/,  '%3B')
    url = url.gsub(/</,  '%3C')
    url = url.gsub(/=/,  '%3D')
    url = url.gsub(/>/,  '%3E')
    url = url.gsub(/\?/, '%3F')
    url = url.gsub(/@/,  '%40')
    url = url.gsub(/\[/, '%5B')
    url = url.gsub(/\\/, '%5C')
    url = url.gsub(/\]/, '%5D')
    url = url.gsub(/\^/, '%5E')
    url = url.gsub(/_/,  '%5F')
    url = url.gsub(/`/,  '%60')
    url = url.gsub(/{/,  '%7B')
    url = url.gsub(/\|/, '%7C')
    url = url.gsub(/}/,  '%7D')
    url = url.gsub(/~/,  '%7E')
    return url
  end
  def releaseEscapeUrl(url)
    if url.nil?
	return url
    end
    url = url.gsub(/%20/, ' ')
    url = url.gsub(/%21/, '!')
    url = url.gsub(/%22/, '"')
    url = url.gsub(/%23/, '#')
    url = url.gsub(/%24/, '$')
    url = url.gsub(/%25/, '%')
    url = url.gsub(/%26/, '&')
    url = url.gsub(/%27/, '\'')
    url = url.gsub(/%28/, '(')
    url = url.gsub(/%29/, ')')
    url = url.gsub(/%2A/, '*')
    url = url.gsub(/%2B/, '+')
    url = url.gsub(/%2C/, ',')
    url = url.gsub(/%2D/, '-')
    url = url.gsub(/%2E/, '.')
    #url = url.gsub(/%2F/, '/')
    url = url.gsub(/%3A/, ':')
    url = url.gsub(/%3B/, ';')
    url = url.gsub(/%3C/, '<')
    url = url.gsub(/%3D/, '=')
    url = url.gsub(/%3E/, '>')
    url = url.gsub(/%3F/, '?')
    url = url.gsub(/%40/, '@')
    url = url.gsub(/%5B/, '[')
    #url = url.gsub(/%5C/, '\\')
    url = url.gsub(/%5D/, ']')
    url = url.gsub(/%5E/, '^')
    url = url.gsub(/%5F/, '_')
    url = url.gsub(/%60/, '`')
    url = url.gsub(/%7B/, '{')
    url = url.gsub(/%7C/, '|')
    url = url.gsub(/%7D/, '}')
    url = url.gsub(/%7E/, '~')
    return url
  end
  helper_method :releaseEscapeUrl
end
