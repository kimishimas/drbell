class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

	before_action :basic
	private def basic
			user = request.env['REMOTE_USER']
			session[:staff_id] = user
	end
end
