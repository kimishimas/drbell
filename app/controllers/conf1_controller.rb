class Conf1Controller < ApplicationController
  
  #CONF101
  def index
    @id = CallPace.first.cp_id.to_s
    if @id.empty? then
      @message = '発呼ペースが１件も登録されていません。'
    else
      redirect_to :action => 'show', :id => @id and return
    end
    
  end
  
  #CONF101
  def show
    @id = params[:id]
    if !params[:message].blank?
      @message = params[:message]
    end
    callpace = CallPace.all.order(:cp_id)
    @callpacehash = Array.new
    callpace.each do |calle|
      @callpacehash << [calle.description, calle.cp_id]
    end
    @callpace = callpace.find(@id)
    render :action => 'index'
  end
  
  #CONF101
  def update
    @id = params[:id]
    
    callpace = CallPace.find_by(cp_id: @id)
    if (callpace == nil)
      @message = 'DBの書き込みでエラーが発生しました。'
      @callpace = callpace
      render :action => 'index' and return
    end 
    callpace.cp_0 = cp_params[:cp_0]
    callpace.cp_1 = cp_params[:cp_1]
    callpace.cp_2 = cp_params[:cp_2]
    callpace.cp_3 = cp_params[:cp_3]
    callpace.cp_4 = cp_params[:cp_4]
    callpace.cp_5 = cp_params[:cp_5]
    callpace.cp_6 = cp_params[:cp_6]
    callpace.cp_7 = cp_params[:cp_7]
    callpace.cp_8 = cp_params[:cp_8]
    callpace.cp_9 = cp_params[:cp_9]
    callpace.cp_10 = cp_params[:cp_10]
    callpace.cp_11 = cp_params[:cp_11]
    callpace.cp_12 = cp_params[:cp_12]
    callpace.cp_13 = cp_params[:cp_13]
    callpace.cp_14 = cp_params[:cp_14]
    callpace.cp_15 = cp_params[:cp_15]
    callpace.cp_16 = cp_params[:cp_16]
    callpace.cp_17 = cp_params[:cp_17]
    callpace.cp_18 = cp_params[:cp_18]
    callpace.cp_19 = cp_params[:cp_19]
    callpace.cp_20 = cp_params[:cp_20]
    callpace.cp_21 = cp_params[:cp_21]
    callpace.cp_22 = cp_params[:cp_22]
    callpace.cp_23 = cp_params[:cp_23]
    
    cp_list = CallPace.all.order(:cp_id)
    @callpacehash = Array.new
    cp_list.each do |calle|
    	@callpacehash << [calle.description, calle.cp_id]
    end
    
    if !callpace.valid?
    	@callpace = callpace	
    	render :action => 'index' and return
    end 
    
    if callpace.save
    	flash[:true] = '更新しました'
    else
    	flash[:false] = 'DBの書き込みでエラーが発生しました。'
    end
    
    @callpace = callpace
    render :action => 'index' and return
  end
  
  #CONF101
  def destroy
    @id = params[:id]
    
      @callpace = CallPace.find(params[:id])
    cp_name = @callpace.description
    
    # gateway table check
    if @callpace.gateways.count != 0
    	@message = cp_name + ' が登録されているOGW/HGWが存在する為、削除できません。'
    	redirect_to :action => 'show', :id => @id, :message => @message and return
    end
    
    # delete
    if @callpace.destroy
    	flash[:true] = cp_name + ' を削除しました。'
    	@id = CallPace.first.cp_id.to_s
    else
    	flash[:flase] = 'DBの書き込みでエラーが発生しました。'
    end
    
    redirect_to :action => 'show', :id => @id
  end
  
  #CONF102
  def new
    @callpace = CallPace.new
    cp_n_val = 60			# 発呼ペースdefault値
    @callpace.cp_0 = cp_n_val
    @callpace.cp_1 = cp_n_val
    @callpace.cp_2 = cp_n_val
    @callpace.cp_3 = cp_n_val
    @callpace.cp_4 = cp_n_val
    @callpace.cp_5 = cp_n_val
    @callpace.cp_6 = cp_n_val
    @callpace.cp_7 = cp_n_val
    @callpace.cp_8 = cp_n_val
    @callpace.cp_9 = cp_n_val
    @callpace.cp_10 = cp_n_val
    @callpace.cp_11 = cp_n_val
    @callpace.cp_12 = cp_n_val
    @callpace.cp_13 = cp_n_val
    @callpace.cp_14 = cp_n_val
    @callpace.cp_15 = cp_n_val
    @callpace.cp_16 = cp_n_val
    @callpace.cp_17 = cp_n_val
    @callpace.cp_18 = cp_n_val
    @callpace.cp_19 = cp_n_val
    @callpace.cp_20 = cp_n_val
    @callpace.cp_21 = cp_n_val
    @callpace.cp_22 = cp_n_val
    @callpace.cp_23 = cp_n_val
  
  end
  
  #CONF102
  def create
    max_id = CallPace.all.reverse_order.first.cp_id
    callpace = CallPace.new(cp_params)
    if max_id.blank? then max_id = 0 end
    callpace.cp_id = max_id + 1
    callpace.is_default = 0
    callpace.description = callpace.description.strip
    
    if !callpace.valid? then
      @callpace = callpace
      render :action => 'new' and return
    end
    
    if callpace.save then
      flash[:true] = callpace.description + ' を新規登録しました。'
      redirect_to :action => 'show', :id => callpace.cp_id
    else
      flash[:flase] = 'DBの書き込みでエラーが発生しました。'
      @callpace = callpace
      render :action => 'new'
    end
  end
  
  
  private
    def find_id_conf
      @callpace = CallPace.find(params[:id])
    end
    
  def cp_params
    params.require(:call_pace).permit(:description,:cp_0,:cp_1,:cp_2,:cp_3,:cp_4,:cp_5,:cp_6,:cp_7,:cp_8,:cp_9,:cp_10,:cp_11,:cp_12,:cp_13,:cp_14,:cp_15,:cp_16,:cp_17,:cp_18,:cp_19,:cp_20,:cp_21,:cp_22,:cp_23)
  end
end
