class Conf2Controller < ApplicationController
	#records per page
	PER = 10

  def history
  	 @files_name = Dir["/var/log/drbell/*"]
  	 #@files_name = Dir["public/*"]
  end
  def download
		@navail_csv = NavailList.all.order("na_id")
		send_data render_to_string, filename: 'jishukulist.txt', type: :csv
  end
  def downloadlog
        send_file params[:id], type: "application/txt", x_sendfile: true
  end
  def index
		if !params[:message].blank?
			@message = params[:message]
		end

		@keyword = ''
		@navaillist = Kaminari.paginate_array(NavailList.all.order("na_id")).page(params[:page]).per(PER)
		@navail_new = NavailList.new()
		@count = NavailList.count
  end

	def search
		if !params[:keyword]
			redirect_to :action => 'index' and return
		else
			keyword = params[:keyword]
			keyword.gsub!(/'/) do |m|
				'\'' + m
			end
			keyword.gsub!(/%/, "\\%")
			keyword.gsub!(/_/, "\\_")
		end
		@navaillist = 
			Kaminari.paginate_array(NavailList.where("(telnum_exp LIKE '%" + keyword + "%') OR (remarks LIKE '%" + keyword + "%')").order("na_id")).page(params[:page]).per(PER)
		@navail_new = NavailList.new()
		@count = NavailList.count
		render :action => 'index'
	end

  def update
    #更新
    navail_list = params[:navail]
    ids = navail_list.keys
    navail_list.each {|key, value|
      value.delete("is_delete")
      value.delete("na_id")
    }
    attributes = navail_list.values.map {|u| u.permit(:telnum_exp, :remarks, :enabled_flg) }
    begin
      if NavailList.update(ids, attributes)
	flash[:true] = '更新しました。'
      else
        flash[:false] = '自粛リストが重複しています。'
      end
    rescue
      flash[:false] = '自粛リストが重複しています。'
    end
    @navaillist = Kaminari.paginate_array(NavailList.all.order("na_id")).page(params[:page]).per(PER)
    @navail_new = NavailList.new()
		@count = NavailList.count
    render :action => 'index' and return
  end

  def new
    logger.debug("--- comming NEW !! ---")
    #idをセット
    latest_navail = NavailList.all.reverse_order.first
    if latest_navail.nil?
      max_id = 0
    else
      max_id = latest_navail.na_id
    end
    navail_new = NavailList.new(na_params)
    navail_new.na_id = max_id + 1
    logger.debug("na_id = #{navail_new.na_id}")
    logger.debug("telnum_exp = #{navail_new.telnum_exp}")
    logger.debug("remarks = #{navail_new.remarks}")
    logger.debug("enabled_flg = #{navail_new.enabled_flg}")
    if !navail_new.valid?
      logger.debug("--- params are invalid ---")
      @navaillist = Kaminari.paginate_array(NavailList.all.order("na_id")).page(params[:page]).per(PER)
      @navail_new = navail_new
      logger.debug(navail_new.errors.full_messages)
			@count = NavailList.count
      render :action => 'index' and return
    end
    logger.debug("--- params are VARID ---")
   
    begin
      if navail_new.save 
        flash[:true] = '自粛リストの登録が完了しました。'
        @navail_new = NavailList.new()
      else
        flash[:false] = '自粛リストが重複しています。'
      end
    rescue
      flash[:false] = '自粛リストが重複しています。'
    end
    @navaillist = Kaminari.paginate_array(NavailList.all.order("na_id")).page(params[:page]).per(PER)
		@count = NavailList.count
    redirect_to conf2_path
  end

	def destroy
		navail_list = params[:navail]
		condition = ""
		navail_list.each { |key, navail|
			if navail[:is_delete] == "false" then
				next
			else
				condition += "na_id = #{navail[:na_id]} OR "
			end
		}
		if condition != ''
			condition = condition.slice(0..-4)
			logger.debug(condition)
			if NavailList.where(condition).delete_all
				flash[:true] = '削除しました。'
			else
				flash[:flase] = 'の書き込みでエラーが発生しました。'
			end
		else
			flash[:flase] = '削除するレコードを選択してください。'
		end
		@navail_new = NavailList.new()
		@navaillist = Kaminari.paginate_array(NavailList.all.order("na_id")).page(params[:page]).per(PER)
		@count = NavailList.count
		render :action => 'index'
	end

	#登録用パラメータ取得
	def na_params
		params.require(:navail_list).permit(:telnum_exp, :remarks, :enabled_flg)
	end

end
