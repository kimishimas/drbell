class ConfController < ApplicationController
  before_action :set_calling_controller, only: [:show, :update]
  #CONF001
  def index
    @calling_controllers = CallingController.all.order("cc_id asc")
  end

  #CONF002
  def show
    @call_paces = CallPace.all
  end

  #CONF003
  def update
    if @calling_controller.update(calling_controller_params)
      redirect_to conf_done_path(@calling_controller)
    else
      render :show
    end
  end

  def done
    @calling_controllers = CallingController.find(params[:id])
  end

  private

  def set_calling_controller
    @calling_controller = CallingController.find(params[:id])
  end

  def calling_controller_params
    params.require(:calling_controller).permit(gateways_attributes: [:id, :channels, :cp_id, gw_telnums_attributes: [:id, :telnum, :_destroy]])
  end
end
