class Jobm1Controller < ApplicationController
	before_action :set_job_month, only: [:show, :edit, :update, :destroy]

  #JOBM102
  def new
    @job_month = Job.new
    @errors_msg = params[:alert]
  end

  #JOBM103
  def confirm
    uploaded_files = []
    Job.transaction do
      @job = Job.new

      # JobIDの生成
      @job.job_id = ActiveRecord::Base.connection.select_value('SELECT gen_jobid()')

      @job.type_cd = 0 # ジョブ種別コード(月次)
      @job.status = 0 # ジョブ状態(待機)
      @job.description = job_month_params[:description] # ジョブ名
      @job.staff_nm = job_month_params[:staff_nm] # 登録者名
      @job.staff_id = session[:staff_id] # 登録者ID
      @job.priority = 0 # 優先度
      @job.save!

      total_file = 0
      total_line = 0
      line_count = 0
      job_month_params[:file_csv].each do |file|
        # ファイルのアップロード
        new_filepath = File.join(UPDATA, @job.job_id.to_s + "_" + file.original_filename)
        uploaded_files.push(new_filepath)
        File.open(new_filepath, "wb") {|f|
          File.open(file.tempfile) {|rf|
            rf.each_line do |line|
              f.write(line)
            end
            line_count = rf.lineno
            rf.close
          }
          f.close
        }
        total_line += line_count

        # 電話番号データファイルの登録
        @telnum_files = TelnumFile.new
        @telnum_files.job_id = @job.job_id
        @telnum_files.filename = file.original_filename
        @telnum_files.loaded = false
        @telnum_files.save!
        total_file += 1
      end

      @job.upd_recs = total_line
      @job.save!

      if params[:op_start]
				#他の月次ジョブのstatuチェック
				running_jobs = Job.where(type_cd: 0).where.not(job_id: @job.job_id, status: 3..4).where.not(status: 0).count
				@is_start = false
				if running_jobs == 0
	        @ope_requests = OpeRequest.new
	        @ope_requests.staff_id = session[:staff_id] # 登録者ID
	        @ope_requests.ope_type_cd = 1 # 作業種別コード(実行)
	        @ope_requests.ope_job_id = @job.job_id # 作業対象ジョブID
	        @ope_requests.save!
					@is_start = true
				end
      end
    end
    if params[:op_start]
			if @is_start
      	redirect_to :action => "finish", :id => @job.job_id, :notice => 'request'
			else
				redirect_to :action => "finish", :id => @job.job_id, :notice => 'notrun'
			end
    else
      redirect_to :action => "finish", :id => @job.job_id
    end
    rescue => e
    render plain: e.message
    uploaded_files.each do |filename|
      if File.exist?(filename)
        File.unlink(filename)
      end
    end
  end

  def finish
    @job = Job.find(params[:id])
    @description = @job.description
    @staff_nm = @job.staff_nm
    @request = params[:notice]
    @files = []
    @telnumfile = TelnumFile.where(job_id: @job.job_id)
    @telnumfile.each do |file|
      filepath = File::join(UPDATA, @job.job_id.to_s + "_" + file[:filename])
      open(filepath){|f|
        while f.gets; end
        @line_count = f.lineno
      }
      @files.push({:filename => file[:filename], :line_count => @line_count})
    end
   if @request == 'request'
     @notice = "以下の通り、設定と対象リストの登録が完了し、\n処理が開始されました。"
   else
     @notice = "以下の通り、設定と対象リストの登録が完了しました。"
   end
  end

  #JOBM104 not page view
  def create
  end

  #JOBM105
  def show
  end

  def update
  end
  
  def destroy
  end

    private
    	def set_job_month
        @job_month = Job.find(params[:id])
    	end

    	def job_month_params
              params.require(:job).permit(:job_id, :description, :staff_nm, {file_csv: []})
    	end
end
