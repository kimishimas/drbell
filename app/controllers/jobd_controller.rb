class JobdController < ApplicationController
  #before_action :set_, only: [:show, :edit, :update, :destroy]
	before_action :set_job_month, only: [:show, :edit, :update, :destroy]

  #JOBD101
  def new
		@job = Job.find(params[:main_id])
		if @job.type_cd == '受託ジョブ'
			redirect_to "/main/#{@job.job_id}" and return
		end
		@errors_msg = params[:alert]
  end

  #JOBD102
  def confirm
		uploaded_files = []
		total_file = 0
		TelnumFile.transaction do
			@job = Job.find(params[:main_id])

			total_line = @job.upd_recs
			line_count = 0
			file_params[:file_csv].each do |file|
				#ファイルのアップロード
				new_filepath = File.join(UPDATA, @job.job_id.to_s + "_" + file.original_filename)
				uploaded_files.push(new_filepath)
				File.open(new_filepath, "wb") {|f|
					File.open(file.tempfile) {|rf|
						rf.each_line do |line|
							f.write(line)
						end
						line_count = rf.lineno
						rf.close
					}
					f.close
				}
				total_line += line_count

				#電話番号データファイルの登録
				@telnum_files = TelnumFile.new
				@telnum_files.job_id = @job.job_id
				@telnum_files.filename = file.original_filename
				@telnum_files.loaded = false
				if !@telnum_files.valid?
					redirect_to :action => "new", :main_id => @job.job_id, :alert => @telnum_files.errors.full_messages and return
				end
				@telnum_files.save!
				total_file += 1
				 if @job.status == 3
					@job.status = 2
				 end
				
				#アップロード件数を追加
				@job.upd_recs = total_line
				@job.save!
			end
		end
		redirect_to :action => "finish", :main_id => @job.job_id, :file_count => total_file
		rescue => e
		render plaint: e.message
		uploaded_files.each do |filename|
			if File.exist?(filename)
				File.unlink(filename)
			end
		end
  end

	def finish
		@job = Job.find(params[:main_id])
		@files = []
		@telnumfile = TelnumFile.where(job_id: @job.job_id).order("created_at DESC").limit(params[:file_count])
		@telnumfile.each do |file|
			filepath = File::join(UPDATA, @job.job_id.to_s + "_" + file[:filename])
			open(filepath){|f|
				while f.gets; end
				@line_count = f.lineno
			}
			@files.push({:filename => file[:filename], :line_count => @line_count})
		end
	end

  #JOBD103 not page view
  def create
  end

  #JOBD104
  def show
  end

  def destroy
  end

  private
  def set_
    #@ = .find(params[:id])
  end

  def file_params
    params.permit({file_csv: []})
  end
end
