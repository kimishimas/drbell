class JobmController < ApplicationController
  before_action :set_job, only: [:show, :edit, :destroy]

  #jobm002
  def new
    @job = Job.new
  end

  #jobm004 not page view
  def create
    @job = Job.new(job_params.merge({
      job_id: ActiveRecord::Base.connection.select_value('SELECT gen_jobid()'), # ジョブID生成
      type_cd: Job.type_cds['受託ジョブ'], # 固定値 1
      staff_id: 'drbell', # ダイジェスト認証のユーザー名を取得
      status: 0, # 状態の初期値は待機（0）
      inner_pri: 0,
      created_at: Time.current
    }))

    file = params[:file_csv][0]
    filename = file.original_filename
    path = UPDATA + @job.job_id.to_s + "_" + filename
    telnum_file = @job.telnum_files.new(filename: filename, loaded: false, created_at: Time.current)

    open(file.tempfile.path){|f|
      while f.gets; end
      @line_count = f.lineno
      @job.upd_recs = @line_count
    }

    ActiveRecord::Base.transaction do
      @job.save!
      telnum_file.save!
    end
    FileUtils.mv(file.tempfile, path)
    FileUtils.chmod(0664, path)

    @notice = '以下の通り、設定と対象リストの登録が完了しました。'
    render :done
  rescue => e
    @notice = '登録に失敗しました。'
    render :new
  end

  def execute
    @job = Job.find(params[:id])

    @ope_requests = OpeRequest.new
    @ope_requests.staff_id = @job.staff_id # 登録者ID
    @ope_requests.ope_type_cd = 1 # 作業種別コード(実行)
    @ope_requests.ope_job_id = @job.job_id # 作業対象ジョブID

    unless @ope_requests.save
      flash[:notice] = '即時実行できませんでした。'
    end

    redirect_to main_index_path
  end

  def show
    @notice = params[:notice]
  end

  def destroy
    @job.destory
  end

  private
  def set_job
    @job = Job.find(params[:id])
  end

  def job_params
    p = params.require(:job).permit(:job_id, :description, :staff_nm, :priority)
    p['priority'] = p['priority'].to_i
    p
  end
end
