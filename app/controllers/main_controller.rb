class MainController < ApplicationController

#	helper_method :map_priority
#	helper_method :map_type_cd

  JOBTYPE_MONTH = 0
  JOBTYPE_ACCESS = 1

  NAVAIL_TASK_SUMMARY = -1

  #JOBM001
  def index
    @job_acces = Job.where(type_cd: JOBTYPE_ACCESS).order({ created_at: :desc }, :created_at).includes(:task_summaries, :job_status)
    @acces_progress_list = {}
    @acces_navail_list = {}
    @job_acces.each do |f|
      #進捗率
      progress = ActiveRecord::Base.connection.select_value("SELECT get_job_prograte('#{f.job_id}')")
      @acces_progress_list["#{f.job_id}"] = progress
      #自粛リスト
      navail = f.task_summaries.detect { |j| j.carrier_type_cd == NAVAIL_TASK_SUMMARY }
      if navail.blank? || navail.ok_recs.blank?
        @acces_navail_list["#{f.job_id}"] = 0
      else
        @acces_navail_list["#{f.job_id}"] = navail.ok_recs
      end
    end
    @month_progress_list = {}
    @month_navail_list = {}
    @job_month = Job.where(type_cd: JOBTYPE_MONTH).order({ created_at: :desc }, :created_at).includes(:task_summaries, :job_status)
    @job_month.each do |f|
      #進捗率
      progress = ActiveRecord::Base.connection.select_value("SELECT get_job_prograte('#{f.job_id}')")
      @month_progress_list["#{f.job_id}"] = progress
      #自粛リスト
      navail = f.task_summaries.detect { |j| j.carrier_type_cd == NAVAIL_TASK_SUMMARY }
      if navail.blank? || navail.ok_recs.blank? 
        @month_navail_list["#{f.job_id}"] = 0
      else
        @month_navail_list["#{f.job_id}"] = navail.ok_recs
      end
    end
    #回線エラー
    @errors = TaskSummary.where.not("carrier_type_cd = -1").group(:job_id).sum(:err_recs)

    #自粛エラー
    @navail_errors = TaskSummary.where("carrier_type_cd = -1").group(:job_id).sum(:err_recs)

    #自粛リスト
    @navail_ok = TaskSummary.where("carrier_type_cd = -1").group(:job_id).sum(:ok_recs)

  end

  #JOBD001
  def show
    @job = Job.find_by(job_id: params[:id])
		#削除された or 不正なjob_idの場合、indexへリダイレクト
		if @job.nil?
			redirect_to main_index_path and return
		end
    @file = TelnumFile.order(created_at: :desc).find_by(job_id: @job.job_id)
    @progress = ActiveRecord::Base.connection.select_value("SELECT get_job_prograte('#{@job.job_id}')")
    @navail = TaskSummary.find_by(job_id: @job.job_id, carrier_type_cd: -1)
    #残り件数
    @left = @job.alltask_recs
    @job.task_summaries.each do |f|
      if f.ok_recs.nil? 
        @left = (@job.alltask_recs - 0) if f.carrier_type_cd != -1
      else
        @left -= f.ok_recs if f.carrier_type_cd != -1
        @left -= f.err_recs if f.carrier_type_cd != -1
      end
    end
    @left -= @navail.ok_recs if @navail.nil? == false && @navail.ok_recs.nil? == false 
    logger.debug(@left)
    #調査開始時刻
    #経過時間
    if (@job.started_at.blank?)
      @startdatetime = ""
      @elapsed = ""
    else
      @startdatetime = @job.started_at.to_s(:datetime)
			if @job.job_status.status_cd == 3 || @job.job_status.status_cd == 4
				elapsed_sec = @job.finished_at - @job.started_at
			else
	      elapsed_sec = (Time.new - @job.started_at)
			end
      elapsed_day = (elapsed_sec / (60 * 60 * 24)).floor
      elapsed_sec -= elapsed_day * 60 * 60 * 24
      elapsed_hour = (elapsed_sec / (60 * 60)).floor
      elapsed_sec -= elapsed_hour * 60 * 60
      elapsed_min = (elapsed_sec / 60).floor
      @elapsed = "#{elapsed_day}日 #{elapsed_hour}時間 #{elapsed_min}分"
    end
    #完了予定
    if @job.job_status.status_cd == 1 || @job.job_status.status_cd == 2
    	@estimated = ActiveRecord::Base.connection.select_value("SELECT get_job_expdt('#{@job.job_id}')")
			date_ary = @estimated.split(" ")
			year = date_ary[0]
			year = year.gsub!(/-/, '/')
			time = date_ary[1]
			time = time.split(".")
			time = time[0]
			@estimated = year + " " + time
		elsif @job.job_status.status_cd == 3 || @job.job_status.status_cd == 4
			if !@job.finished_at.blank?
				@estimated = @job.finished_at.to_s(:datetime)
			else
				@estimated = ""
			end
		else
			@estimated = ""
		end

    #キャリア種別
    @carriers = {}
    TaskSummary.where(job_id: @job.job_id).each do |f|
      if f.carrier_type_cd == -1 then
        next
      else
        carrier = Carrier.where(type_cd: f.carrier_type_cd).first
        @carriers.store(carrier.description, f.ok_recs)
      end
    end
  rescue ActiveRecord::StatementInvalid
    @message = "DB Error" #kimishima
  end

  #JOBA001
  def jobcommand
    command = params[:command].to_i
    job_id = params[:main_id]
    @job = Job.find(job_id)
		#月次ジョブの場合、他のジョブが実行中でないかチェックする
		running_jobs = Job.where(type_cd: 0).where.not(job_id: @job.job_id, status: 3..4).where.not(status: 0).count
		logger.debug("jobs count: #{running_jobs} type_cd: #{@job.type_cd}")
		if command == 1 && @job.type_cd == '月次ジョブ' && running_jobs > 0
			logger.debug("can not start running")
			command = 0
		else
    	#ope_requestへinsert
    	if command == 6
				if @job.status == 0
					command = 11
				elsif @job.status == 2
					command = 12
				elsif @job.status == 3
					command = 13
				elsif @job.status == 4
					command = 14
				end
			end
    	request = OpeRequest.new(staff_id: session[:staff_id], ope_type_cd: command, ope_job_id: job_id)
    	request.save
		end
		logger.debug("\n\ncommand: #{command}\n")
    #3=完了と4=確定はボタンなし
    case command
    when 1 then
      @command = '実行'
    when 2 then
      @command = '中断'
    when 5 then
      @command = '再調査準備'
    when 6 then
      @command = '削除'
    when 7 then
      @command = '再開'
		when 11 then
			@command = '削除'
		when 12 then
			@command = '削除'
		when 13 then
			@command = '削除'
		when 14 then
			@command = '削除'
		when 0 then
			@command = '0'
    else
    end
  end

end
